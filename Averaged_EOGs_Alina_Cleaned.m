%% Set up enviornment
clc 
clear 

datadir = 'D:\PROJECT_SSVEP\SSVEP0\Epochs_hEOG_plots';
scriptdir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\ProcessingScripts';
addpath('D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\ProcessingScripts\Shared_Functions');


%% Plot the averaged EOGs

file_list = dir(strcat(datadir, '\*set'));

x = -1000 : 1000/256 : 7999;
Conditions=[81;82;83;84;91;92;93;94]; %triggers for conds
ConditionNames = {'NegL2';'NegL25';'NegR2';'NegR25';'PosL2';'PosL25';'PosR2';'PosR25'};
leftconds = [1,2,5,6];
rightconds = [3,4,7,8];
n_samples = 2304;

alina_y_left = zeros(n_samples,  numel(file_list));
alina_y_right = zeros(n_samples,  numel(file_list));

total_number_of_epochs = 0;
total_left_epochs = 0;
total_right_epochs = 0;

for i_ss = 1 : numel(file_list)

        EEG = pop_loadset('filename',file_list(i_ss).name,'filepath',file_list(i_ss).folder);
        total_number_of_epochs = total_number_of_epochs + EEG.trials;
        
        EEG2 = pop_selectevent(EEG,'type',Conditions(leftconds));
        total_left_epochs = total_left_epochs + EEG2.trials;
        alina_y_left(:, i_ss) = mean(squeeze(EEG2.data),2);
        
        EEG2 = pop_selectevent(EEG,'type',Conditions(rightconds));
        total_right_epochs = total_right_epochs + EEG2.trials;
        alina_y_right(:, i_ss) = mean(squeeze(EEG2.data),2);
end
   
%%
    
figure('Name', 'Old Alina Data hEOG plots', 'Color', 'w', 'Units', 'Normalized');
subplot(1,1,1);
alina_left_averaged =  mean(alina_y_left,2);
plot(x, alina_left_averaged)
hold on
alina_right_averaged =  mean(alina_y_right,2);
plot(x, alina_right_averaged) 
box off 
set(gca, 'tickdir', 'out');
xlim([-1000 8000])  
ylim([-5 15])
xlabel('Time points /ms') 
ylabel('EOG Amplitude /mV') 
legend('Left','Right')
title("Alina Cleaned Averaged hEOGs")    

%% Extra info 

max_of_grand_average_old_alina_left = max(abs(alina_left_averaged))
max_of_grand_average_old_alina_right = max(abs(alina_right_averaged))

epochs_per_subject = total_number_of_epochs / numel(file_list)
left_epochs_per_subject = total_right_epochs / numel(file_list)
right_epochs_per_subject = total_left_epochs / numel(file_list)
