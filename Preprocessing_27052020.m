%%%%%%%%%%%%%%%%%%%        SSVEP PROJECT 2020         %%%%%%%%%%%%%%%%%%%  
% Created by Lna.A - November 15 2020

% Preprocessing steps:
% 1.	Select BDF files to preprocess
% 2.	Downsampling to 256 Hz 
% 3.	Add channel locations (electrodes & coordinates)
% 4.	Add EOG channels & remove empty channels (eegF_Bipolarize)
% 5.	DETREND (remove linear drifts) (eegF_Detrend)
% 6.	Segmentation all conditions (from -1000 to + 8000 relative to cue)
% 7.	Baseline correction (from -1000 to -500 ms)
% 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Set up enviornment
clc 
clear 

datadir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\RawBDF\';
savedir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\Tmp\Preprocessed\';
scriptdir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\ProcessingScripts\';
cd 'D:\EEGLAB\eeglab2019_1\'
eeglab;
addpath('D:\EEGLAB\eeglab2019_1\plugins\FASTER\')
addpath('D:\EEGLAB\eeglab2019_1\plugins\firfilt1.5.1\')
addpath( 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\ProcessingScripts\Shared_Functions')
%% Preprocessing Pipeline

%Extract files 
cd(datadir)
addpath(scriptdir)
Inputdir = dir('*.bdf');
Filenames = {};
is_filtering = 1; % Set this flag to 0 if all datasets are desired
filter = "059";   % Pick the file to process if filtering is on
for i=1:numel(Inputdir)
    i_name = Inputdir(i).name;
    i_name_participant_id = i_name(1:3); % The first three characters are always the participant ID
    if ~is_filtering      
        Filenames = [Filenames, i_name];    
    elseif is_filtering && strcmp(i_name_participant_id, filter)        
        Filenames = [Filenames, i_name];    
    end
end
    
ss_num = size(Filenames,2);
fprintf('We found %d participant to preprocess\n', ss_num)

% Name variables
% triggers for first stim in RSVPs
Conditions=[81;82;83;84;91;92;93;94]; %triggers for conds
Triggers={'81' '82' '83' '84' '91' '92' '93' '94'}; %triggers for conds
Condname={'NegL2';'NegL25';'NegR2';'NegR25';'PosL2';'PosL25';'PosR2';'PosR25'};% labels for conds (target+side+frequency)

% Electrodes of interest
ChannelSelection=[15 23 24 25 52 60 61 62];   %PO9/PO10, P9/P10, P7/8, PO7/PO8
leftpool=[15 23 24 25];
rightpool=[52 60 61 62];

for i=1:length(Filenames) %for ID020 i=[1:11,13:ss_num]   %skip 12 since 015 has no data  
    %STEP 1: Downsampling to 256 Hz.  
        EEG = pop_biosig([datadir, Filenames{i}]);
        EEG = pop_resample(EEG, 256);
    % STEP 2: Edit channel locations
        EEG = pop_chanedit(EEG, 'lookup',[scriptdir,'standard-10-5-cap385.elp']);
    % STEP 3: Add EOG channels and remove Empty channels
        % add EOG channels (vEOG = EXG1-EXG2, hEOG=EXG3-EXG4)    
        EEG = eegF_Bipolarize(EEG);% 
        % remove empty channels 67:70
        EEG = pop_select( EEG,'channel',[1:66]);
        
    % Filter: Low-pass with cutoff at 2.6 Hz
        %EEG = pop_eegfiltnew(EEG, 'locutoff',0,'hicutoff',10,'plotfreqz',1);
    
    % STEP 4: Detrend
        EEG = eegF_Detrend(EEG);
    % STEP 5: Segment
        EEG = pop_epoch( EEG, Triggers, [-1   8]);
    % STEP 6:  Baseline Correction 
        % baseline correction (cue onset = -500 ms)
        EEG = pop_rmbase( EEG, [-1000 -500]); % remove baseline [-500; 0] relative to cue onset)

% save dataset for next step
savename = [strrep(Filenames{i},'.bdf',''),'Preprocessed.set'];
EEG = pop_saveset( EEG, 'filename',savename,'filepath',savedir);

end
    

 %% Separate pipeline for datasets that have odd triggers (512)
     % remove triggers and then run pre-process the data 
     
% triggers for first stim in SSVPs
Conditions=[81;82;83;84;91;92;93;94]; %triggers for conds
Triggers={'81' '82' '83' '84' '91' '92' '93' '94'}; %triggers for conds
Condname={'NegL2';'NegL25';'NegR2';'NegR25';'PosL2';'PosL25';'PosR2';'PosR25'};% labels for conds (target+side+frequency)

% Electrodes of interest
ChannelSelection=[15 23 24 25 52 60 61 62];   %PO9/PO10, P9/P10, P7/8, PO7/PO8
leftpool=[15 23 24 25];
rightpool=[52 60 61 62];


cd(datadir)
addpath(scriptdir)
Inputdir = dir('*.bdf');   
Filenames = {};
FilesWith512 = {"009"}; % Pick the file to fix its triggers and then pre-process
for i = 1:length(FilesWith512)
    for j = 1:numel(Inputdir)
        i_name = Inputdir(j).name;   
        i_name_participant_id = i_name(1:3); % The first three characters are always the participant ID
        if strcmp(i_name_participant_id, FilesWith512{i})        
            Filenames = [Filenames, i_name];    
        end
    end
end
    
ss_num = size(Filenames,2);
fprintf('We found %d participants to preprocess\n', ss_num)

for i= 1:length(Filenames)
    EEG = pop_biosig([datadir, Filenames{i}]);
    oldtrigs = struct2table(EEG.event);
    newtrigs = oldtrigs;
 
    for k=1:size(newtrigs,1)
        if  newtrigs.type(k) > 511
            newtrigs.type(k)=newtrigs.type(k)-512;
        end
    end

    newtrigs(newtrigs.type == 0,:) = []; % Remove rows with type column value equal to zero
    EEG.event = table2struct(newtrigs);

    EEG = pop_resample(EEG, 256);
    % STEP 2: Edit channel locations
    EEG = pop_chanedit(EEG, 'lookup',[scriptdir,'standard-10-5-cap385.elp']);
    % STEP 3: Add EOG channels and remove Empty channels
    % add EOG channels (vEOG = EXG1-EXG2, hEOG=EXG3-EXG4)    
    EEG = eegF_Bipolarize(EEG);% 
    % remove empty channels 67:70
    EEG = pop_select( EEG,'channel',[1:66]);
    
    % Filter: Low-pass with cutoff at 2.6 Hz
    EEG = pop_eegfiltnew(EEG, 'locutoff',0,'hicutoff',10,'plotfreqz',1);
    
    % STEP 4: Detrend
    EEG = eegF_Detrend(EEG);
    % STEP 5: Segment
    EEG = pop_epoch( EEG, Triggers, [-1   8]);
    % STEP 6:  Baseline Correction 
    % baseline correction (cue onset = -500 ms)
    EEG = pop_rmbase( EEG, [-1000 -500]); % remove baseline [-500; 0] relative to cue onset)
    
    % Remove any duplicated epochs     
    events = struct2table(EEG.event);
    [~, unique_urevent_indeces, ~] = unique(events.urevent);
    unique_epochs = unique(events.epoch(unique_urevent_indeces));
    duplicated_epochs = setdiff(events.epoch, unique_epochs);% Reject marked epochs 
    enable_auto_confirmation = 0;  % To enable the auto confimation, put this as the third argument of pop_rejepoch()
    EEG = pop_rejepoch(EEG, duplicated_epochs, enable_auto_confirmation);
    
    % save dataset for next step
    savename = [strrep(Filenames{i},'.bdf',''),'Preprocessed.set'];
    EEG = pop_saveset( EEG, 'filename',savename,'filepath',savedir);
end