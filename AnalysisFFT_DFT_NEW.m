
%% Set up enviornment
clear all 
datadir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\CleanedData'; 
savedir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\Analysis';
scriptdir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\ProcessingScripts\Shared_Functions';
cd 'D:\EEGLAB\eeglab2019_1\'
eeglab;
addpath('D:\EEGLAB\eeglab2019_1\plugins\FASTER\')
addpath('D:\EEGLAB\eeglab2019_1\plugins\firfilt1.5.1\')
addpath('D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\ProcessingScripts\Shared_Functions')

%%                     ***** 1. AVERAGING *****
% Compute SSVEPS per subject and condition
% Load cleaned and FASTER processed EEG files .set
% Output:
cd(datadir)
addpath(scriptdir)
Inputdir = dir('*.set');
Filenames = {};
ParticipantIds = {};
for i=1:numel(Inputdir)
    i_name = Inputdir(i).name;
    Filenames{i} = i_name;
    ParticipantIds{i} = i_name(1:3);
end

% Load the meta-data for each participant (i.e. group allocation)
participant_meta_data = readtable(savedir + "\new_participant_meta_data.csv",'ReadVariableNames',true,'Format','%s %s');
AVG_index = find(ismember(participant_meta_data.Group, 'AVGP'));
NVG_index = find(ismember(participant_meta_data.Group, 'NVGP'));

% Just a check to make sure that we didn't accidentally load the files in
% the wrong order.
assert(isequal(participant_meta_data.ID,  strip(ParticipantIds','left','0')), "Participant data files were loaded in a bad order.")


ss_num = size(Filenames,2);
fprintf('We found %d participants to preprocess\n', ss_num)

%Set Subject Variables
Conditions=[81;82;83;84;91;92;93;94]; %triggers for conds
Condname={'NegL2';'NegL25';'NegR2';'NegR25';'PosL2';'PosL25';'PosR2';'PosR25'};% labels for conds (target+side+frequency)

%%%% 1.2 Averaging EEG Data
eeglab;
for i = 1:ss_num
    EEG=pop_loadset(Filenames{i})
    % Calculate mean of trials in each condition
    for ConditionNr=1:numel(Conditions)
        EEG_temp=pop_selectevent(EEG,'type',Conditions(ConditionNr));
        %create a matrix to store each condition, electrodes, time,and subjects
        AllAvg(ConditionNr,:,:,i)=mean(EEG_temp.data,3);
    end
end

%%                        ***** 2. FFT *****
% Run FFT per subject and plot grand averages
% Prepare SSVEP data for 
%    - stats (individual averages)
%    - plots (grand-averages)
    
%%%% 2.1 setup
%----- Define EEG variables -------------------
freqConds = [2,2.5];% frequency conditions
sRate = 256;% sampling frequency
nPoints = 2^14;% resolution: number of sampling points (range 0-256 Hz)
TRange = [0.5 7.5];% time range in seconds
Freqs = (0:nPoints-1)*sRate/nPoints;% corresponding frequency range    
SRange = (TRange+1)*2304/9 + 1;% time range in sampling points ([-1 +8] <=> 2304)    
SPRange = SRange(1):SRange(2);% time range of data to analyse
NbSamplingPoints = numel(SPRange); % the nb of sampling points in that time range
     

%%%% 2.2 Compute INDIVIDUAL FFTs and extract peak DFTs amplitudes
%----- Extract FFT and DFT for each subject and condition -----------------
% Load random data set for structure (?)

for ConditionNr = 1:numel(Conditions)
    
    % Load individual EEG averages into EEG.data
    EEG.data = permute(AllAvg(ConditionNr,:,:,:),[2 3 4 1]); 

EEG= eegF_Detrend(EEG,TRange); % remove drift within range

    for SubjectNr = 1:size(EEG.data,3)
        % FFT over all frequency spectrum
        % FFT contains both amplitude and phase information for each wave)
        % Use "abs" to obtain the absolute amplitude (magnitude)
        % Don't forget to scale the results as described in Exercise 4!
        FFTData(ConditionNr,SubjectNr,:,:) = abs(fft(EEG.data(:,SPRange,SubjectNr),...
            nPoints,2))*2/NbSamplingPoints;% nPoints (2^14) can be replaced by [];       
        
        % DFT over specific frequency
        for freq = 1:numel(freqConds)
            EEG2 = eegF_DFT(EEG,freqConds(freq),TRange); 
            DFTData(ConditionNr,SubjectNr,:,freq)= EEG2.data(:,1000,SubjectNr);
        end
    end
    
end


ChannelSelection = [15 23 24 25 52 60 61 62];% channels: PO9/PO10, P9/P10, P7/8, PO7/PO8
freqNames = {'2 Hz';'2.5 Hz'};
SRange = (TRange+1)*2304/9 + 1;% time range in sampling points ([-1 +8] <=> 2304)    
SPRange = SRange(1):SRange(2);% time range of data to analyse
NbSamplingPoints = numel(SPRange); % the nb of sampling points in that time range
SideFreqConds = {'Left_2_Hz';'Left_25_Hz';'Right_2_Hz';'Right_25_Hz'}; % averaged conditons


%% Save data for SnR plots

lowPlottingFrequency = 1;
highPlottingFrequency = 9;
frequency_indeces = (Freqs >= lowPlottingFrequency) & (Freqs <= highPlottingFrequency);
xAxis = Freqs(frequency_indeces);
Frequencies = xAxis';

FrequencySpectrumTableColumns = {'Frequencies', Condname{:}};

for i = 1:numel(ParticipantIds)
    frequency_domain_data = ...
        squeeze(...% remove 1-size dimensions
            mean(FFTData(:, i, ChannelSelection, frequency_indeces), 3))';

    frequency_domain_data = ...
        array2table([Frequencies, frequency_domain_data], ...
                    'VariableNames', FrequencySpectrumTableColumns);

    file_name = strcat(ParticipantIds{i}, '_new_periodogram.csv');
    writetable(frequency_domain_data, strcat('D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\PipelineComparison\', file_name));
end
%% PLOT 1: periodogram per group (average NEG/POS & Left/Right)
%----- Plot FFT per group and attended Frequency --------------------------
% AVERAGE LEFT AND RIGHT POSTERIOR CHANNELS

figure1 = figure('Name','Periodogram per group', 'Color', 'w');

% Prepare the X axis 
lowPlottingFrequency = 1;
highPlottingFrequency = 9;
xAxis = Freqs((Freqs >= lowPlottingFrequency) & (Freqs <= highPlottingFrequency));% Time points
Frequencies = xAxis';
xScale = (0:2^14-1)*(sRate/nPoints); % nPoints makes curve smoother

groupnames = {'AVGPs', 'NVGPs'};
sidenames = {'Left', 'Right'};
grouped_participant_ids = {AVG_index, NVG_index};

is_for_snr = false;
if is_for_snr
    id_list = cell(1, numel(grouped_participant_ids{1}) + numel(grouped_participant_ids{2}));
    for i = 1:numel(id_list)
        id_list{i} = i;
    end
else
    id_list = grouped_participant_ids;
end

for i = 1:length(id_list)
    %Ss = 3;% subjects from group gr
    subjects = id_list{i};
    for cond = 1:size(freqNames,1)
        
        % Prepare the Y axis 
        yAxis = squeeze(...% remove 1-size dimensions
                mean(...
                    mean(...
                        mean(...
                            FFTData([cond:2:8],...% average NEG and POS for each cond
                                    subjects,...% subjects from group gr
                                    ChannelSelection,...% all channels
                                    (Freqs >= lowPlottingFrequency) & (Freqs <= highPlottingFrequency) ...
                            ),...% plot only specific Frequency Range
                        3),...% average across selected channels
                    1),...% average NEG and POS
                2));% average across subjects

        if strcmp(freqNames{cond}, '2 Hz')
           Amplitudes_2Hz = squeeze(yAxis);
        else
           Amplitudes_2p5Hz = squeeze(yAxis);
        end
        if ~is_for_snr
            subplot(2,1,i);
            title(groupnames{i});
            plot(xAxis, squeeze(yAxis));
            xlim([1 7])
            xlabel('Frequency(Hz)');
            ylabel('mV');
            set(gca,'FontSize',16);
            hold all;

            legend(freqNames);
            box off
            legend boxoff
        end
    end
    if is_for_snr
        periodograms = table(Frequencies, Amplitudes_2Hz, Amplitudes_2p5Hz);
        file_name = strcat(ParticipantIds{i}, '_new_periodogram.csv');
        writetable(periodograms, strcat('D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\PipelineComparison\', file_name));
    end
end
%% PLOT 2: Peak topographies per group (per side and frequency, averaging NEG & POS)
%----- Plot DFT per group, Side and Frequency -----------------------------

for i = 1:length(grouped_participant_ids)  
    figure2_3 = figure('Color', 'w', 'Units', 'Normalized', 'OuterPosition', [0, 0.2, 0.65, 0.2 ]);
    sgt = sgtitle(groupnames{i});
    sgt.FontSize = 20;
    subjects = grouped_participant_ids{i};
    p = 1; % subplot index
    
    for cond = 1:size(SideFreqConds,1)% for each condition

        for fr = 1:size(freqConds,2)% loop across frequencies
            
            %TOPOPLOT
            EEG.data=squeeze(mean(...
                                    mean(DFTData(cond:4:8,...% extract NEG and POS
                                        subjects,...% subjects from group gr)...
                                        :,...% all channels
                                        fr),...% specific frequency Peak
                                    1),...% average NEG & POS ;
                                2));% average across subjects

            % draw plot 
            subplot(2,4,p);
            title(freqNames{fr});
            topoplot(EEG.data,EEG.chanlocs,'conv','on','maplimits', 'maxmin');%'conv on' limits interpolation of the electric field into electrode-free space                        
            set(gcf,'color','w');
            hold all;
     
            p = p + 1;
        end               
    end
    
    colorbar('peer', gca, 'Position',[0.1 0.35 0.01 0.3], ...
             'LineWidth', 1);
         
    annotation(figure2_3,...
            'textbox',[0.01 0.70 0.1 0.1],...% Left, Top
            'String',{'Attend: LEFT'},...
            'FontWeight','bold',...
            'FontSize',14,...
            'FitBoxToText','on',...
            'LineStyle','none'); 
        
    annotation(figure2_3,...
            'textbox',[0.01 0.20 0.1 0.1],...% Left, Top
            'String',{'Attend: RIGHT'},...
            'FontWeight','bold',...
            'FontSize',14,...
            'FitBoxToText','on',...
            'LineStyle','none');         

    annotation(figure2_3,...
            'textbox',[0.25 0.90 0.1 0.1],...% Left, Top
            'String',{'Attend 2 Hz'},...
            'FontWeight','bold',...
            'FontSize',14,...
            'FitBoxToText','on',...
            'LineStyle','none'); 
        
    annotation(figure2_3,...
            'textbox',[0.65 0.90 0.1 0.1],...% Left, Top
            'String',{'Attend 2.5 Hz'},...
            'FontWeight','bold',...
            'FontSize',14,...
            'FitBoxToText','on',...
            'LineStyle','none');  
end
 
    
%% Plot 3 replication
assert(length([AVG_index; NVG_index]) == length(unique([AVG_index; NVG_index])), "Error, participant exists in both groups");

Group_index = [AVG_index; NVG_index]; % Index of gamer and non-gamer
LRcond = [1,5; 2,6; 3,7; 4,8]; %leftlow, lefthigh, rightlow, righthigh
BrainSide = [52 60 61 62; 15 23 24 25]; %rightpool, leftpool
freqNames = {'2 Hz';'2.5 Hz'};
ColAVG = [0.8078    0.1333    0.1333];
ColNAVG = [0, 0.4470, 0.8410];
addpath('D:\PROJECT_SSVEP\SSVEP2\ProcessingScripts\Shared_Functions')

AttendedData = nan(ss_num, 4);
UnattendedData = nan(ss_num, 4);
i=1; j=1;
for i_BrainSide = 1:2 % 1= look at left stimuli (right brain), 2= look at right stimuli (left brain)

    i_LRcond = 1;  
    if i_BrainSide == 1              %Low frequency Left Attended
        i_Hz = 1; 
        temp = DFTData(LRcond(i_LRcond,:), :, BrainSide(i_BrainSide,:), i_Hz);
        AttendedData(:,i) = mean(mean(temp(:,:,:),3),1)'; 
        i=i+1;
    else
        i_Hz = 2;                   %else: High frequency Left Unattended
        temp = DFTData(LRcond(i_LRcond,:), :, BrainSide(i_BrainSide,:), i_Hz);
        UnattendedData(:,j) = mean(mean(temp(:,:,:),3),1)'; 
        j=j+1;
    end 

    i_LRcond = 4;           
    if i_BrainSide == 1              %Low frequency Left Unattended
        i_Hz = 1; 
        temp = DFTData(LRcond(i_LRcond,:), :, BrainSide(i_BrainSide,:), i_Hz);
        UnattendedData(:,j) = mean(mean(temp(:,:,:),3),1)';  
        j=j+1;
    else
        i_Hz = 2;                    %else: High frequency Left Attended
        temp = DFTData(LRcond(i_LRcond,:), :, BrainSide(i_BrainSide,:), i_Hz);
        AttendedData(:,i) = mean(mean(temp(:,:,:),3),1)'; 
        i=i+1;
    end  

    i_LRcond = 2; 
    if i_BrainSide == 1              %High frequency Left Attended
        i_Hz = 2; 
        temp = DFTData(LRcond(i_LRcond,:), :, BrainSide(i_BrainSide,:), i_Hz);
        AttendedData(:,i) = mean(mean(temp(:,:,:),3),1)'; 
        i=i+1;
    else
        i_Hz = 1;                    %else: Low  frequency Right Unttended
        temp = DFTData(LRcond(i_LRcond,:), :, BrainSide(i_BrainSide,:), i_Hz);
        UnattendedData(:,j) = mean(mean(temp(:,:,:),3),1)';  
        j=j+1;
    end  

    i_LRcond = 3;  
    if i_BrainSide == 1              %High frequency Left Unattended
        i_Hz = 2;
        temp = DFTData(LRcond(i_LRcond,:), :, BrainSide(i_BrainSide,:), i_Hz);
        UnattendedData(:,j) = mean(mean(temp(:,:,:),3),1)';  
        j=j+1;
    else
        i_Hz = 1;                    %else: Low frequency Right Attended
        temp = DFTData(LRcond(i_LRcond,:), :, BrainSide(i_BrainSide,:), i_Hz);
        AttendedData(:,i) = mean(mean(temp(:,:,:),3),1)';     
        i=i+1;        
    end  
end

%
figure('Name','Gamer Left 2Hz', 'Color', 'w', 'Units', 'Normalized', 'OuterPosition', [0, 0.05, 1.1, 0.9 ]);
%strings for figure
groupname = {'nogroup';'nogroup';'nogroup';'nogroup'};
condnames = {'Left';'Left';'Right';'Right'};
freqnames = {'2 Hz';'2.5 Hz';'2 Hz';'2.5 Hz'};

for k =1:4
    subplot(1,4,k);
    allData = {AttendedData(:,k); UnattendedData(:,k)}; 
    group = [ones(size(AttendedData(:,k))); 2 * ones(size(AttendedData(:,k)))];

    h = boxplot(cell2mat(allData),group,'Notch','on','OutlierSize',0.01,'Symbol','+','Labels',{'Attended','Unattended'}); %hide outlier mark with Outlier size 0.01
    lines = findobj(gcf, 'type', 'line', 'Tag', 'Median');
    set(lines, 'Color', 'k');hold on
    x1=CreateSemiGaussianJitter(AttendedData(:,k));
    x2=CreateSemiGaussianJitter(UnattendedData(:,k));
    for i = 1:numel(allData)
        plot(x1, AttendedData(:,k),'o','MarkerEdgeColor',ColAVG)
        text(x1,AttendedData(:,k),ParticipantIds,'VerticalAlignment','bottom','HorizontalAlignment','right')
        plot(1+x2, UnattendedData(:,k),'o','MarkerEdgeColor',ColNAVG) %plotting pre-test values NAVG
        text(1+x2,UnattendedData(:,k),ParticipantIds,'VerticalAlignment','bottom','HorizontalAlignment','right')
    end
    
    plot([x1 1+x2]', [AttendedData(:,k) UnattendedData(:,k)]', 'k:') %connect dots
    title(strcat(groupname{k},{' '},condnames{k},{' '}, freqnames{k}));
    xlabel('Condition'); ylabel('Amplitude') 
    ax = gca; ax.FontSize = 13;
    xlim([0.5 2.5])
    
    %LimY = ylim; LimY = [round(LimY(1)+0.05,1) round(LimY(2)+0.05,1) ]; ylim(LimY); set(gca,'ytick', LimY)
    LimY = ylim; LimY =  round(LimY+[-.1 .1],1); ylim(LimY); set(gca,'ytick', LimY)
    legend('Attended','Unattended', 'Location', 'northeast');
    legend boxoff
    box off
    set(gca, 'tickdir', 'out');
    [h,p] = ttest(AttendedData(:,k),UnattendedData(:,k));
    cohendval = computeCohen_d(AttendedData(:,k),UnattendedData(:,k), 'paired');
    txt_p = ['pval = ', num2str(p,'%.4f')];
    txt_d = ['d = ', num2str(cohendval,'%.4f')];
    text(0.35,0.08, txt_p, 'Units', 'Normalized', 'Fontsize', 10)
    text(0.35,0.04, txt_d, 'Units', 'Normalized', 'Fontsize', 10)
end
                 

%% Format data for ANOVA

all_group_options = {'NVGP', 'AVGP'};
all_attention_options = {'Attended'; 'Unattended'};
all_experimenter_options = {'Alina', 'new'};
all_frequency_options = {'2 Hz', '2.5 Hz'};
all_emotion_options = {'Neg', 'Pos'};
all_side_options = {'left', 'right'};
leftPool = [15 23 24 25]; % left electrodes for right attended and unattended frequencies
rightPool = [52 60 61 62]; % right electrodes for left attended and unattended frequencies

% Parallel arrays for going through the conditions
Conditions = {'NegL2';'NegL25';'NegR2';'NegR25';'PosL2';'PosL25';'PosR2';'PosR25'};
Side = {'left', 'left', 'right', 'right', 'left', 'left', 'right', 'right'};
Freq = {'2 Hz', '2.5 Hz', '2 Hz', '2.5 Hz', '2 Hz', '2.5 Hz', '2 Hz', '2.5 Hz'};
Emo = {'Neg', 'Neg', 'Neg', 'Neg', 'Pos', 'Pos', 'Pos', 'Pos'};
AttFreq = [2, 2.5, 2, 2.5, 2, 2.5, 2, 2.5];
UnattFreq = [2.5, 2, 2.5, 2, 2.5, 2, 2.5, 2];
AttendedElectrodes = {rightPool, rightPool, leftPool, leftPool, rightPool, rightPool, leftPool, leftPool};
UnattendedElectrodes = {leftPool, leftPool, rightPool, rightPool, leftPool, leftPool, rightPool, rightPool};

data4stats = [];
for subject_number = 1:numel(participant_meta_data.ID)
    if  isempty(strfind(participant_meta_data.ID{subject_number},'s')) == 1
        experimenter = 'new';
    else 
        experimenter = 'Alina';
    end 
    
    group = participant_meta_data.Group(subject_number);
    id = participant_meta_data.ID(subject_number);
        
    for condition = 1:length(Conditions)
        attended_frequency = freqConds == AttFreq(condition);
        unattended_frequency = freqConds == UnattFreq(condition);
        
        attended_amplitude = mean(DFTData(condition, subject_number, AttendedElectrodes{condition}, attended_frequency), 3);
        unattended_amplitude = mean(DFTData(condition, subject_number, UnattendedElectrodes{condition}, unattended_frequency), 3);
        
        attended_row = [group, id, Freq(condition), Side(condition), 'Attended', Emo(condition), experimenter, attended_amplitude];
        unattended_row = [group, id, Freq(condition), Side(condition), 'Unattended', Emo(condition), experimenter, unattended_amplitude];
        data4stats = [data4stats; attended_row; unattended_row];
    end  
end

data4stats = cell2table(data4stats, ... 
	'VariableNames',{'groupData', 'subjData', 'freqData', 'sideData', 'attData', 'emoData', 'Experimenter', 'Amplitude'});
writetable(data4stats, strcat(savedir,'\data4stats_combined.csv'));

%% Box plots for amplitude comparisons across groups and conditions

figure('Name','Amplitudes Comparisons', 'Color', 'w', 'Units', 'Normalized', 'OuterPosition', [0, 0.05, 1.1, 0.9 ]);
for group_index = 1:numel(all_group_options)
    
    participant_indeces = find(ismember(participant_meta_data.Group, all_group_options(group_index)));
    for condition = 1:(numel(Conditions) / 2) % Only need half of the conditions because negative and positive amplitudes are averaged

        amplitudes = [];
        attention_info = {};
        for participant_index = 1:numel(participant_indeces)
            participant_id = participant_meta_data.ID(participant_indeces(participant_index));
            new_amplitudes = GetAmplitudeFromTable(data4stats, participant_id, Freq(condition), Side(condition), all_attention_options);
            amplitudes = [amplitudes; new_amplitudes];
            attention_info = [attention_info; all_attention_options];
        end

        if strcmp(all_group_options{group_index}, 'NVGP') == 1
            nvgp_subplot_indeces = 1:2:7;
            subplot(4, 2, nvgp_subplot_indeces(condition));
        else
            avgp_subplot_indeces = 2:2:8;
            subplot(4, 2, avgp_subplot_indeces(condition));
        end
        boxplot(amplitudes, attention_info, 'Symbol', '+');
        hold on
        scatter_x = CreateSemiGaussianJitter(amplitudes) + repmat([0; 1], 0.5 * numel(amplitudes), 1);
        scatter(scatter_x, amplitudes);
        xlabel('Condition'); 
        ylabel('Amplitude') 
        title(all_group_options(group_index) + " " + Freq(condition) + " " + Side(condition));
    end
end

%% Run the ANOVA

anovan(data4stats.Amplitude, ...
       {data4stats.groupData, data4stats.freqData, data4stats.sideData, data4stats.attData, data4stats.emoData}, ... 
       'model', 'interaction', 'varnames', {'Group', 'Frequency', 'Side', 'Attention', 'Emotion'});
   
%% Follow-up on the interaction Group x Attention 

pick_attended = 'Attended';
pick_unattended = 'Unattended';

desired_row_indeces = GetIndecesFromTable(data4stats, participant_meta_data.ID, all_frequency_options, all_side_options, ...
                                          pick_unattended, all_emotion_options, all_experimenter_options, all_group_options);
                                      
anovan(data4stats.Amplitude(desired_row_indeces), ...
       {data4stats.groupData(desired_row_indeces), data4stats.freqData(desired_row_indeces), ...
       data4stats.sideData(desired_row_indeces), data4stats.emoData(desired_row_indeces)}, ... 
       'model', 'interaction', 'varnames', {'Group', 'Frequency', 'Side', 'Emotion'})
   
%% Box plots of amplitudes by experimenter

pick_alina = 'Alina';
pick_new = 'new';
pick_avgp = 'AVGP';
pick_nvgp = 'NVGP';

alina_avgp_row_indeces = GetIndecesFromTable(data4stats, participant_meta_data.ID, all_frequency_options, all_side_options, ...
                                             all_attention_options, all_emotion_options, pick_alina, pick_avgp);    
alina_nvgp_row_indeces = GetIndecesFromTable(data4stats, participant_meta_data.ID, all_frequency_options, all_side_options, ...
                                             all_attention_options, all_emotion_options, pick_alina, pick_nvgp);    
                                         
new_avgp_row_indeces = GetIndecesFromTable(data4stats, participant_meta_data.ID, all_frequency_options, all_side_options, ...
                                           all_attention_options, all_emotion_options, pick_new, pick_avgp);                                  
new_nvgp_row_indeces = GetIndecesFromTable(data4stats, participant_meta_data.ID, all_frequency_options, all_side_options, ...
                                           all_attention_options, all_emotion_options, pick_new, pick_nvgp);
                
% Plot                                       
boxplot(data4stats.Amplitude, data4stats.Experimenter, 'Symbol', '+');
hold on
scatter_x = CreateSemiGaussianJitter(data4stats.Amplitude(new_avgp_row_indeces));
scatter(scatter_x, data4stats.Amplitude(new_avgp_row_indeces), 'blue');
hold on
scatter_x = CreateSemiGaussianJitter(data4stats.Amplitude(new_nvgp_row_indeces));
scatter(scatter_x, data4stats.Amplitude(new_nvgp_row_indeces), 'red');
hold on
scatter_x = 1 + CreateSemiGaussianJitter(data4stats.Amplitude(alina_avgp_row_indeces));
scatter(scatter_x, data4stats.Amplitude(alina_avgp_row_indeces), 'blue');
hold on
scatter_x = 1 + CreateSemiGaussianJitter(data4stats.Amplitude(alina_nvgp_row_indeces));
scatter(scatter_x, data4stats.Amplitude(alina_nvgp_row_indeces), 'red');

% number of AVGPs and NVGPs for Alina
text(2.1,3,strcat('AVGPs: 15'));
text(2.1,2.9,strcat('NVGPs: 14'));

% number of AVGPs and NVGPs for new
text(1.1,3,strcat('AVGPs: 20'));
text(1.1,2.9,strcat('NVGPs: 17'));

% Format the plot
xlabel('Experimenter'); 
ylabel('Amplitude'); 
legend({'AVGP', 'NVGP'});

%% Box plots of amplitudes by experimenter with groups unmixed 

pick_alina = 'Alina';
pick_new = 'new';
pick_avgp = 'AVGP';
pick_nvgp = 'NVGP';

alina_avgp_row_indeces = GetIndecesFromTable(data4stats, participant_meta_data.ID, all_frequency_options, all_side_options, ...
                                             all_attention_options, all_emotion_options, pick_alina, pick_avgp);    
alina_nvgp_row_indeces = GetIndecesFromTable(data4stats, participant_meta_data.ID, all_frequency_options, all_side_options, ...
                                             all_attention_options, all_emotion_options, pick_alina, pick_nvgp);    
                                         
new_avgp_row_indeces = GetIndecesFromTable(data4stats, participant_meta_data.ID, all_frequency_options, all_side_options, ...
                                           all_attention_options, all_emotion_options, pick_new, pick_avgp);                                  
new_nvgp_row_indeces = GetIndecesFromTable(data4stats, participant_meta_data.ID, all_frequency_options, all_side_options, ...
                                           all_attention_options, all_emotion_options, pick_new, pick_nvgp);

box_plot_data = [
 num2cell(data4stats.Amplitude(alina_avgp_row_indeces)), repelem({'Alina AVGP'}, sum(alina_avgp_row_indeces),1); ...
 num2cell(data4stats.Amplitude(alina_nvgp_row_indeces)), repelem({'Alina NVGP'}, sum(alina_nvgp_row_indeces),1); ...
 num2cell(data4stats.Amplitude(new_avgp_row_indeces)), repelem({'New AVGP'}, sum(new_avgp_row_indeces),1); ...
 num2cell(data4stats.Amplitude(new_nvgp_row_indeces)), repelem({'New NVGP'}, sum(new_nvgp_row_indeces),1)];                     

box_plot_data = cell2table(box_plot_data, ... 
	'VariableNames',{'Amplitude' 'experimenterAndGroup'});
                                       
boxplot(box_plot_data.Amplitude, box_plot_data.experimenterAndGroup, 'Symbol', '+');
xlabel('Experimenter and Group'); 
ylabel('Amplitude'); 

%% Box plots of Amplitude by Side
                                    
boxplot(data4stats.Amplitude, data4stats.sideData, 'Symbol', '+');
xlabel('Side'); 
ylabel('Amplitude'); 

%% Box plots of Amplitude by Emotion

boxplot(data4stats.Amplitude, data4stats.emoData, 'Symbol', '+');
xlabel('Emotion'); 
ylabel('Amplitude'); 

%% Time course plot for PO10 across participants

PO10_electrode = 52;
freq_2hz_conditions = find(contains(Freq,'2 Hz'));
freq_2p5hz_conditions = find(contains(Freq,'2.5 Hz'));
time = (([1 : length(AllAvg)] * 1000)/ sRate) - 1000;

electrode_grand_average_for_2hz_conditions = ...
    squeeze(mean(AllAvg(freq_2hz_conditions, PO10_electrode, :, :), [1,4]));

electrode_grand_average_for_2p5hz_conditions = ...
    squeeze(mean(AllAvg(freq_2p5hz_conditions, PO10_electrode, :, :), [1,4]));

subplot(2,1,1)
plot(time, electrode_grand_average_for_2hz_conditions)
xlabel("Time [ms]");
ylabel("Potential [uV]");
title("Time course of grand-average SSVEP for 2 Hz Conditions");
hold on
plot([0 0],[-6 6], 'r')
text(100, 5, 'Stream Onset', 'HorizontalAlignment', 'left', 'Color', 'r')
hold on
plot([-500 -500],[-6 6], 'g')
text(-400, 5, 'Cue', 'HorizontalAlignment', 'left', 'Color', 'g')
hold on
ylim([-6,6]);

subplot(2,1,2)
plot(time, electrode_grand_average_for_2p5hz_conditions)
xlabel("Time [ms]");
ylabel("Potential [uV]");
title("Time course of grand-average SSVEP for 2.5 Hz Conditions");
hold on
plot([0 0],[-6 6], 'r')
text(100, 5, 'Stream Onset', 'HorizontalAlignment', 'left', 'Color', 'r')
hold on
plot([-500 -500],[-6 6], 'g')
text(-400, 5, 'Cue', 'HorizontalAlignment', 'left', 'Color', 'g')
hold on
ylim([-6,6]);

%% Time course plot for PO10 across participants by side and frequency for NVGP and AVGP

PO10_electrode = 52;
freq_2hz_left_conditions = intersect(find(contains(Freq,'2 Hz')), find(contains(Side,'left')));
freq_2hz_right_conditions = intersect(find(contains(Freq,'2 Hz')), find(contains(Side,'right')));
freq_2p5hz_left_conditions = intersect(find(contains(Freq,'2.5 Hz')), find(contains(Side,'left')));
freq_2p5hz_right_conditions = intersect(find(contains(Freq,'2.5 Hz')), find(contains(Side,'right')));
time = (([1 : length(AllAvg)] * 1000)/ sRate) - 1000;

avg_electrode_grand_average_for_2hz_left_conditions = ...
    squeeze(mean(AllAvg(freq_2hz_left_conditions, PO10_electrode, :, AVG_index), [1,4]));
nvg_electrode_grand_average_for_2hz_left_conditions = ...
    squeeze(mean(AllAvg(freq_2hz_left_conditions, PO10_electrode, :, NVG_index), [1,4]));

avg_electrode_grand_average_for_2hz_right_conditions = ...
    squeeze(mean(AllAvg(freq_2hz_right_conditions, PO10_electrode, :, AVG_index), [1,4]));
nvg_electrode_grand_average_for_2hz_right_conditions = ...
    squeeze(mean(AllAvg(freq_2hz_right_conditions, PO10_electrode, :, NVG_index), [1,4]));

avg_electrode_grand_average_for_2p5hz_left_conditions = ...
    squeeze(mean(AllAvg(freq_2p5hz_left_conditions, PO10_electrode, :, AVG_index), [1,4]));
nvg_electrode_grand_average_for_2p5hz_left_conditions = ...
    squeeze(mean(AllAvg(freq_2p5hz_left_conditions, PO10_electrode, :, NVG_index), [1,4]));

avg_electrode_grand_average_for_2p5hz_right_conditions = ...
    squeeze(mean(AllAvg(freq_2p5hz_right_conditions, PO10_electrode, :, AVG_index), [1,4]));
nvg_electrode_grand_average_for_2p5hz_right_conditions = ...
    squeeze(mean(AllAvg(freq_2p5hz_right_conditions, PO10_electrode, :, NVG_index), [1,4]));

subplot(4,2,1)
plot(time, avg_electrode_grand_average_for_2hz_left_conditions)
xlabel("Time [ms]");
ylabel("Potential [uV]");
title("Time course of AVGP grand-average SSVEP for 2 Hz left conditions");
hold on
plot([0 0],[-6 6], 'r')
text(100, 5, 'Stream Onset', 'HorizontalAlignment', 'left', 'Color', 'r')
hold on
plot([-500 -500],[-6 6], 'g')
text(-400, 5, 'Cue', 'HorizontalAlignment', 'left', 'Color', 'g')
hold on
ylim([-6,6]);

subplot(4,2,3)
plot(time, nvg_electrode_grand_average_for_2hz_left_conditions)
xlabel("Time [ms]");
ylabel("Potential [uV]");
title("Time course of NVGP grand-average SSVEP for 2 Hz left conditions");
hold on
plot([0 0],[-6 6], 'r')
text(100, 5, 'Stream Onset', 'HorizontalAlignment', 'left', 'Color', 'r')
hold on
plot([-500 -500],[-6 6], 'g')
text(-400, 5, 'Cue', 'HorizontalAlignment', 'left', 'Color', 'g')
hold on
ylim([-6,6]);

subplot(4,2,2)
plot(time, avg_electrode_grand_average_for_2hz_right_conditions)
xlabel("Time [ms]");
ylabel("Potential [uV]");
title("Time course of AVGP grand-average SSVEP for 2 Hz right conditions");
hold on
plot([0 0],[-6 6], 'r')
text(100, 5, 'Stream Onset', 'HorizontalAlignment', 'left', 'Color', 'r')
hold on
plot([-500 -500],[-6 6], 'g')
text(-400, 5, 'Cue', 'HorizontalAlignment', 'left', 'Color', 'g')
hold on
ylim([-6,6]);

subplot(4,2,4)
plot(time, nvg_electrode_grand_average_for_2hz_right_conditions)
xlabel("Time [ms]");
ylabel("Potential [uV]");
title("Time course of NVGP grand-average SSVEP for 2 Hz right conditions");
hold on
plot([0 0],[-6 6], 'r')
text(100, 5, 'Stream Onset', 'HorizontalAlignment', 'left', 'Color', 'r')
hold on
plot([-500 -500],[-6 6], 'g')
text(-400, 5, 'Cue', 'HorizontalAlignment', 'left', 'Color', 'g')
hold on
ylim([-6,6]);

subplot(4,2,5)
plot(time, avg_electrode_grand_average_for_2p5hz_left_conditions)
xlabel("Time [ms]");
ylabel("Potential [uV]");
title("Time course of AVGP grand-average SSVEP for 2.5 Hz left conditions");
hold on
plot([0 0],[-6 6], 'r')
text(100, 5, 'Stream Onset', 'HorizontalAlignment', 'left', 'Color', 'r')
hold on
plot([-500 -500],[-6 6], 'g')
text(-400, 5, 'Cue', 'HorizontalAlignment', 'left', 'Color', 'g')
hold on
ylim([-6,6]);

subplot(4,2,7)
plot(time, nvg_electrode_grand_average_for_2p5hz_left_conditions)
xlabel("Time [ms]");
ylabel("Potential [uV]");
title("Time course of NVGP grand-average SSVEP for 2.5 Hz left conditions");
hold on
plot([0 0],[-6 6], 'r')
text(100, 5, 'Stream Onset', 'HorizontalAlignment', 'left', 'Color', 'r')
hold on
plot([-500 -500],[-6 6], 'g')
text(-400, 5, 'Cue', 'HorizontalAlignment', 'left', 'Color', 'g')
hold on
ylim([-6,6]);

subplot(4,2,6)
plot(time, avg_electrode_grand_average_for_2p5hz_right_conditions)
xlabel("Time [ms]");
ylabel("Potential [uV]");
title("Time course of AVGP grand-average SSVEP for 2.5 Hz right conditions");
hold on
plot([0 0],[-6 6], 'r')
text(100, 5, 'Stream Onset', 'HorizontalAlignment', 'left', 'Color', 'r')
hold on
plot([-500 -500],[-6 6], 'g')
text(-400, 5, 'Cue', 'HorizontalAlignment', 'left', 'Color', 'g')
hold on
ylim([-6,6]);

subplot(4,2,8)
plot(time, nvg_electrode_grand_average_for_2p5hz_right_conditions)
xlabel("Time [ms]");
ylabel("Potential [uV]");
title("Time course of NVGP grand-average SSVEP for 2.5 Hz right conditions");
hold on
plot([0 0],[-6 6], 'r')
text(100, 5, 'Stream Onset', 'HorizontalAlignment', 'left', 'Color', 'r')
hold on
plot([-500 -500],[-6 6], 'g')
text(-400, 5, 'Cue', 'HorizontalAlignment', 'left', 'Color', 'g')
hold on
ylim([-6,6]);


% Other style of plot

figure();

subplot(2,2,1)
h(1) = plot(time, avg_electrode_grand_average_for_2hz_left_conditions);
hold on
h(2) = plot(time, nvg_electrode_grand_average_for_2hz_left_conditions);
xlabel("Time [ms]");
ylabel("Potential [uV]");
title("Time course of grand-average SSVEP for 2 Hz left conditions");
plot([0 0],[-6 6], 'r')
text(100, 5, 'Stream Onset', 'HorizontalAlignment', 'left', 'Color', 'r')
plot([-500 -500],[-6 6], 'g')
text(-400, 5, 'Cue', 'HorizontalAlignment', 'left', 'Color', 'g')
ylim([-6,6]);
legend([h(1), h(2)],'AVGP', 'NVGP');

subplot(2,2,2)
h(1) = plot(time, avg_electrode_grand_average_for_2hz_right_conditions);
hold on
h(2) = plot(time, nvg_electrode_grand_average_for_2hz_right_conditions);
xlabel("Time [ms]");
ylabel("Potential [uV]");
title("Time course of grand-average SSVEP for 2 Hz right conditions");
plot([0 0],[-6 6], 'r')
text(100, 5, 'Stream Onset', 'HorizontalAlignment', 'left', 'Color', 'r')
plot([-500 -500],[-6 6], 'g')
text(-400, 5, 'Cue', 'HorizontalAlignment', 'left', 'Color', 'g')
ylim([-6,6]);
legend([h(1), h(2)],'AVGP', 'NVGP');

subplot(2,2,3)
h(1) = plot(time, avg_electrode_grand_average_for_2p5hz_left_conditions);
hold on
h(2) = plot(time, nvg_electrode_grand_average_for_2p5hz_left_conditions);
xlabel("Time [ms]");
ylabel("Potential [uV]");
title("Time course of grand-average SSVEP for 2.5 Hz left conditions");
plot([0 0],[-6 6], 'r')
text(100, 5, 'Stream Onset', 'HorizontalAlignment', 'left', 'Color', 'r')
plot([-500 -500],[-6 6], 'g')
text(-400, 5, 'Cue', 'HorizontalAlignment', 'left', 'Color', 'g')
ylim([-6,6]);
legend([h(1), h(2)],'AVGP', 'NVGP');

subplot(2,2,4)
h(1) = plot(time, avg_electrode_grand_average_for_2p5hz_right_conditions);
hold on
h(2) = plot(time, nvg_electrode_grand_average_for_2p5hz_right_conditions);
xlabel("Time [ms]");
ylabel("Potential [uV]");
title("Time course of grand-average SSVEP for 2.5 Hz right conditions");
plot([0 0],[-6 6], 'r')
text(100, 5, 'Stream Onset', 'HorizontalAlignment', 'left', 'Color', 'r')
plot([-500 -500],[-6 6], 'g')
text(-400, 5, 'Cue', 'HorizontalAlignment', 'left', 'Color', 'g')
ylim([-6,6]);
legend([h(1), h(2)],'AVGP', 'NVGP');