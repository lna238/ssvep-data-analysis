%%%%%%%%%%%%%%%%%%%        SSVEP PROJECT 2020         %%%%%%%%%%%%%%%%%%%  
% Created by Lna.A - November 2020

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Set up enviornment
clc 
clear 

datadir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\Tmp\Interpolated';
scriptdir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\ProcessingScripts';
savedir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\Tmp\EogCleaned';
group_info = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\HEOG_plots_group_info.csv';
manual_cleaning_information = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\manual_cleaning_information.csv';
cd 'D:\EEGLAB\eeglab2019_1\'
eeglab;

addpath('D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\ProcessingScripts\Shared_Functions');

%% Find and mark the EOG epochs that show eye movements and drifts for a participant

cd(datadir)
file_list = dir('*.set');
file_list = {file_list.name};

% Load the cleaning information for each participant
cleaning_info = readtable(manual_cleaning_information, 'Format','%s %s %s');
participant_ids = cleaning_info.Participant_ID;

% Select participant to clean
participant_id = '059';
clear file_to_clean;
participant_row = find(strcmp(participant_ids, strip(participant_id,'left','0')));
% participant_id = participant_id(1:3);

file_index = find(contains(file_list, participant_id)); 
file_to_clean = file_list(file_index);
EEG = pop_loadset(file_to_clean);

fprintf('Begin manual EOG cleaning for participant %s \n', participant_id)

% Here
%EOG_Filtered = pop_eegfiltnew(EEG, 'locutoff',1,'channels',{'VEOG' 'HEOG'});

enable_saving_marked_epochs = 0;  % To enable saving the marked epochs
%cmd = ['[events_to_reject] = eegplot2trial( TMPREJ, EEG.pnts, EEG.trials);'];
%eegplot(EEG.data,'data2', EOG_Filtered.data, 'command', cmd);
pop_eegplot(EEG, 1, 1, enable_saving_marked_epochs);
waitfor(findobj('parent', gcf, 'string', 'UPDATE MARKS'), 'userdata');

% Format the epochs to be rejected as a string with each number separated
% by a space
epochs_to_reject = find(EEG.reject.rejmanual);
% epochs_to_reject = find(events_to_reject);
epochs_to_reject = sprintf('%i ', epochs_to_reject);
epochs_to_reject = strip(epochs_to_reject,'right',' ');

% Update the table
cleaning_info.Eye_Movement_And_Drift_Epochs_Removed(participant_row) = {epochs_to_reject};

% Update the csv file
option=questdlg("Save epochs to 'manual_cleaning_information.csv'?", 'Save File', 'Yes', 'No','Yes');
if(strcmp(option,'Yes'))
    writetable(cleaning_info, manual_cleaning_information);
end

%% Automatically remove epochs

cd(datadir)
file_list = dir('*.set');
file_list = {file_list.name};

% Load the cleaning information for each participant
cleaning_info = readtable(manual_cleaning_information, 'Format','%s %s %s');
participant_ids = cleaning_info.Participant_ID;

Filenames = {};
is_filtering = 0; % Set this flag to 0 if all datasets are desired
filter = "059";   % Pick the file to process if filtering is on
for i=1:numel(file_list) 
    file = file_list{i};
    file_participant_id = file(1:3); % The first three characters are always the participant ID
    if ~is_filtering     
        assert(numel(cleaning_info.Participant_ID) == numel(file_list), "Not all preprocessed files have entries in 'manual_cleaning_information.csv'");
        Filenames = [Filenames, file];    
    elseif is_filtering && strcmp(file_participant_id, filter)     
        Filenames = [Filenames, file];    
    end
end


% Perform the EOG cleaning
for file_index = 1:numel(Filenames)
    file_to_clean = Filenames{file_index};
    EEG = pop_loadset('filename',file_to_clean,'filepath',datadir);
    current_participant_id = strip(file_to_clean(1:3),'left','0');
    
    participant_index = find(strcmp(cleaning_info.Participant_ID, current_participant_id));
    
    % Get epochs to remove 
    epochs_to_remove = cleaning_info.Eye_Movement_And_Drift_Epochs_Removed{participant_index};
    if(isempty(epochs_to_remove))
        epochs_to_remove = []; % No EOG epochs marked to remove 
    else
        % Convert from space separated strings into an array of numbers
        epochs_to_remove = str2double(split(epochs_to_remove, ' '));
    end
    
    events = struct2table(EEG.event);
    epoch_idx = find(ismember(events.epoch, epochs_to_remove));
    bad_urevents = events.urevent(epoch_idx);
    
    % Reject marked epochs 
    enable_auto_confirmation = 0;  % To enable the auto confimation, put this as the third argument of pop_rejepoch()
    disable_auto_confirmation = 1; % To disable the auto confimation, put this as the third argument of pop_rejepoch()
    EEG_final = pop_rejepoch(EEG, epochs_to_remove, enable_auto_confirmation);
    
    % SAVE 
    savename = strcat(file_to_clean(1:3), '_EogCleaned');
    pop_saveset(EEG_final,'filename',savename,'filepath',savedir);

    % Test that the urevents that were present in the post-FASTER dataset are gone 
    test_EEG = pop_loadset(strcat(savedir, '\', savename, '.set'));
    test_events = struct2table(test_EEG.event);
    remaining_bad_urevents = find(ismember(test_events.urevent, bad_urevents));
    assert(isempty(remaining_bad_urevents), "The desired epochs were not removed."); % Assert that the bad urevents were indeed removed, otherwise give error message
end

%% hEOG plots: Plot every eye movement per condition before and after cleaning for drifts using a function 

file_list_after = dir(strcat(savedir, '\*set'));
file_list_before = dir(strcat(datadir, '\*set'));

First34ParticipantsOfInterestBefore = file_list_before(1:34); 
First34ParticipantsOfInterestAfter = file_list_after(1:34);
Second34ParticipantsOfInterestBefore = file_list_before(35:68);
Second34ParticipantsOfInterestAfter = file_list_after(35:68);

LateralizationPlots(First34ParticipantsOfInterestBefore,"Before Cleaning")
LateralizationPlots(First34ParticipantsOfInterestAfter,"After Cleaning")
LateralizationPlots(Second34ParticipantsOfInterestBefore,"Before Cleaning")
LateralizationPlots(Second34ParticipantsOfInterestAfter,"After Cleaning")

%% hEOG plots: Plot averages for Alina and for the newly collected data

file_list_after = dir(strcat(savedir, '\*set'));
file_list_before = dir(strcat(datadir, '\*set'));

[AlinasParticipantsBefore, NewParticipantsBefore] = SortFilesByExperimenter(file_list_before);
[AlinasParticipantsAfter, NewParticipantsAfter] = SortFilesByExperimenter(file_list_after);

AveragedLateralizationPlots(AlinasParticipantsBefore, NewParticipantsBefore, "Before Cleaning")
AveragedLateralizationPlots(AlinasParticipantsAfter, NewParticipantsAfter, "After Cleaning")

%% hEOG plots: Plot superimposed averages for Alina and for the newly collected data

file_list_after = dir(strcat(savedir, '\*set'));
file_list_before = dir(strcat(datadir, '\*set'));

[AlinasParticipantsBefore, NewParticipantsBefore] = SortFilesByExperimenter(file_list_before);
[AlinasParticipantsAfter, NewParticipantsAfter] = SortFilesByExperimenter(file_list_after);

AveragedSuperimposedLateralizationPlots(AlinasParticipantsBefore, NewParticipantsBefore, "Before Cleaning")
AveragedSuperimposedLateralizationPlots(AlinasParticipantsAfter, NewParticipantsAfter, "After Cleaning")

%% hEOG averages and SD by group for Alina's data, cleaned by Lna 

file_list_after = dir(strcat(savedir, '\*set'));
[AlinasParticipantsAfter, NewParticipantsAfter] = SortFilesByExperimenter(file_list_after);% Load the meta-data for each participant (i.e. group allocation)
participant_group_info = readtable(group_info, 'Format','%s %s');

alina_participants_start_index = find(contains(participant_group_info.ID, 's'));
alina_participants_start_index = alina_participants_start_index(1);

alina_participant_group_info = participant_group_info(alina_participants_start_index:end, {'Group', 'ID'});

nvgp_indeces = strcmp(alina_participant_group_info.Group, 'NVGP');
avgp_indeces = strcmp(alina_participant_group_info.Group, 'AVGP');

figure('Name', strcat('hEOG plots with SDs'), 'Color', 'w', 'Units', 'Normalized');
subplot(2,1,1);
AverageAndSdPlots(AlinasParticipantsAfter(avgp_indeces));
title('Horizontal eye movements - AVGPs') 

subplot(2,1,2);
AverageAndSdPlots(AlinasParticipantsAfter(nvgp_indeces));
title('Horizontal eye movements - NVGPs') 

%% hEOG plots by group and experimenter, showing sds

file_list_after = dir(strcat(savedir, '\*set'));
[AlinasParticipantsAfter, NewParticipantsAfter] = SortFilesByExperimenter(file_list_after);% Load the meta-data for each participant (i.e. group allocation)
participant_group_info = readtable(group_info, 'Format','%s %s');

alina_participants_start_index = find(contains(participant_group_info.ID, 's'));
alina_participants_start_index = alina_participants_start_index(1);

alina_participant_group_info = participant_group_info(alina_participants_start_index:end, {'Group', 'ID'});
new_participant_group_info = participant_group_info(1:alina_participants_start_index - 1, {'Group', 'ID'});

alina_nvgp_indeces = strcmp(alina_participant_group_info.Group, 'NVGP');
alina_avgp_indeces = strcmp(alina_participant_group_info.Group, 'AVGP');
new_nvgp_indeces = strcmp(new_participant_group_info.Group, 'NVGP');
new_avgp_indeces = strcmp(new_participant_group_info.Group, 'AVGP');

figure('Name', strcat('hEOG plots with SDs'), 'Color', 'w', 'Units', 'Normalized');
subplot(2,2,1);
AverageAndSdPlots(AlinasParticipantsAfter(alina_avgp_indeces));
title('Alina Participants Horizontal eye movements - AVGPs') 

subplot(2,2,2);
AverageAndSdPlots(AlinasParticipantsAfter(alina_nvgp_indeces));
title('Alina Participants Horizontal eye movements - NVGPs') 

subplot(2,2,3);
AverageAndSdPlots(NewParticipantsAfter(new_avgp_indeces));
title('New Participants Horizontal eye movements - AVGPs') 

subplot(2,2,4);
AverageAndSdPlots(NewParticipantsAfter(new_nvgp_indeces));
title('New Participants Horizontal eye movements - NVGPs') 
%% hEOG plots by group and side, showing sds

file_list_after = dir(strcat(savedir, '\*set'));
[AlinasParticipantsAfter, NewParticipantsAfter] = SortFilesByExperimenter(file_list_after);% Load the meta-data for each participant (i.e. group allocation)
participant_group_info = readtable(group_info, 'Format','%s %s');

nvgp_indeces = strcmp(participant_group_info.Group, 'NVGP');
avgp_indeces = strcmp(participant_group_info.Group, 'AVGP');

figure('Name', strcat('hEOG plots with SDs'), 'Color', 'w', 'Units', 'Normalized');
subplot(2,1,1);
AverageAndSdPlots(file_list_after(avgp_indeces));
title('Participants Horizontal eye movements - AVGPs') 

subplot(2,1,2);
AverageAndSdPlots(file_list_after(nvgp_indeces));
title('Participants Horizontal eye movements - NVGPs') 
%% hEOG plots: Plot every eye movement per condition before cleaning for drifts 
% 
% file_list = dir(strcat(datadir, '\*set'));
% 
% x = -1000 : 1000/256 : 7999;
% hEOGchannel = 66; %channel of interest
% Conditions=[81;82;83;84;91;92;93;94]; %triggers for conds
% ConditionNames = {'NegL2';'NegL25';'NegR2';'NegR25';'PosL2';'PosL25';'PosR2';'PosR25'};
% leftconds = [1,2,5,6];
% rightconds = [3,4,7,8];
% 
% 
% figure('Name','hEOG plots', 'Color', 'w', 'Units', 'Normalized');
% for i_ss = 1 : numel(file_list)
% 
%     subplot(5,5,i_ss); 
%     EEG = pop_loadset('filename',file_list(i_ss).name,'filepath',file_list(i_ss).folder);
%     EEG2 = pop_selectevent(EEG,'type',Conditions(leftconds));
%     y = mean(squeeze(EEG2.data(hEOGchannel,:,:)),2);
%     plot(x, y)
%     hold on
%     EEG2 = pop_selectevent(EEG,'type',Conditions(rightconds));
%     y = mean(squeeze(EEG2.data(hEOGchannel,:,:)),2);
%     plot(x, y)
%     box off 
%     set(gca, 'tickdir', 'out');
%     xlim([-1000 8000])  
%     xlabel('Time points /ms') 
%     ylabel('EOG Amplitude /mV') 
%     legend('Left)','Right')
%     title(strcat("Participant ",file_list(i_ss).name(1:3))) 
% end 
% 
% figure('Name','hEOG plots', 'Color', 'w', 'Units', 'Normalized');
% for i_ss = 1 : numel(file_list)
% 
%     subplot(4,4,i_ss); 
%     EEG = pop_loadset('filename',file_list(i_ss).name,'filepath',file_list(i_ss).folder);
%     EEG2 = pop_selectevent(EEG,'type',Conditions(leftconds));
%     y = mean(squeeze(EEG2.data(hEOGchannel,:,:)),2);
%     plot(x, y)
%     hold on
%     EEG2 = pop_selectevent(EEG,'type',Conditions(rightconds));
%     y = mean(squeeze(EEG2.data(hEOGchannel,:,:)),2);
%     plot(x, y)
%     box off 
%     set(gca, 'tickdir', 'out');
%     xlim([-1000 8000])  
%     xlabel('Time points /ms') 
%     ylabel('EOG Amplitude /mV') 
%     legend('Left)','Right')
%     title(strcat("Participant ",file_list(i_ss).name(1:3))) 
% end 
% 
% sgtitle(strcat('Participant hEOG average by timepoints'));