function [alina_files,lna_files] = SortFilesByExperimenter(files)
    alina_files = [];
    lna_files = [];
    for index = 1:numel(files)
        if files(index).name(1) == 's'
            alina_files = [alina_files, files(index)];
        else
            lna_files = [lna_files, files(index)];
        end 
    end
end

