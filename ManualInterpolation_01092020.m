%%%%%%%%%%%%%%%%%%%        SSVEP PROJECT 2020         %%%%%%%%%%%%%%%%%%%  
% Created by Lna.A - November 15 2020 
%
% To be run from Biotech PC 

% Manual interpolation steps:
% 1.	Select EEG data file
% 2.	Inspect
% 3.	pop_interpolate
% 4.    Save interpolated dataset
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Set up enviornment
clc 
clear 

datadir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\Tmp\Preprocessed\';
savedir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\Tmp\Interpolated\';
scriptdir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\ProcessingScripts\';
manual_cleaning_information = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\manual_cleaning_information.csv';
cd 'D:\EEGLAB\eeglab2019_1\'
eeglab;
addpath('D:\EEGLAB\eeglab2019_1\plugins\FASTER\')
addpath('D:\EEGLAB\eeglab2019_1\plugins\firfilt1.5.1\')

%% Extract files (re-run)

cd(datadir)
file_list = dir('*.set');

selected_participant_id = "041";
clear file_to_insepct;
for file_index = 1:numel(file_list)
    file_name = file_list(file_index).name;
    file_participant_id = file_name(1:3); % The first three characters are always the participant ID
    if strcmp(file_participant_id, selected_participant_id)        
        file_to_insepct = file_list(file_index); 
        break;
    end
end

EEG = pop_loadset('filename',file_to_insepct.name,'filepath',file_to_insepct.folder);
pop_eegplot( EEG, 1, 1, 1);
eeglab redraw

%% Interpolation

cd(datadir)
file_list = dir('*.set');

% Load the cleaning information for each participant
cleaning_info = readtable(manual_cleaning_information, 'Format','%s %s %s');

Filenames = {};
is_filtering = 1; % Set this flag to 0 if all datasets are desired
filter = "059";   % Pick the file to process if filtering is on
for i=1:numel(file_list) 
    file = file_list(i).name;
    file_participant_id = file(1:3); % The first three characters are always the participant ID
    if ~is_filtering     
        assert(numel(cleaning_info.Participant_ID) == numel(file_list), "Not all preprocessed files have entries in 'manual_cleaning_information.csv'");
        Filenames = [Filenames, file];    
    elseif is_filtering && strcmp(file_participant_id, filter)     
        Filenames = [Filenames, file];    
    end
end
 
% Perform the interpolation
for file_index = 1:numel(Filenames)
    file_to_interpolate = Filenames{file_index};
    EEG = pop_loadset('filename',file_to_interpolate,'filepath',datadir);
    current_participant_id = strip(file_to_interpolate(1:3),'left','0');
    
    participant_index = find(strcmp(cleaning_info.Participant_ID, current_participant_id));
    assert(~isempty(participant_index), "Participant has not been interpolated. Interpolate and save channels to inperpolate in 'manual_cleaning_information.csv'");
    
    % Get channels to inperpolate 
    channels_to_interpolate = cleaning_info.Channels_Interpolated{participant_index};
    
    if(isempty(channels_to_interpolate))
        % Do nothing to the data
    else
        % Convert from space separated strings into an array of numbers
        channels_to_interpolate = str2double(split(channels_to_interpolate, ' '));
        EEG = pop_interp(EEG, channels_to_interpolate, 'spherical');
    end
    savename = [strrep(Filenames{file_index},'.set',''),'Interpolated.set'];
    EEG = pop_saveset(EEG, 'filename',savename,'filepath',savedir);   
end