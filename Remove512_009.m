oldtrigs = struct2table(EEG.event);
newtrigs = oldtrigs;
 
for i=1:size(newtrigs,1)
    if  newtrigs.type(i) > 511
        newtrigs.type(i)=newtrigs.type(i)-512;
    end
end

for i=1:size(newtrigs,1)
    if isequal(newtrigs.type(i),0);
    pos_zero(i) = 1;
    else
        pos_zero(i)=0;
    end
end
idx_zero = find(pos_zero==1)

newtrigs(idx_zero,:) = []

EEG.event = table2struct(newtrigs);