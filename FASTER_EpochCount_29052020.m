%%%%%%%%%%%%%%%%%%%        SSVEP PROJECT 2020         %%%%%%%%%%%%%%%%%%%  
% Created by Kengo.S - May 29 2020 
% Edited by Lna.A - November 2020

% Counting FASTER Epoch rejection
% FASTER is used to automatise removal of bad epochs 
% Each FASTER run outputs a log file with a summary of epochs removed and
% channels interpolated
% This scripts takes the log files and summarises them in a single matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Set up environment
clc 
clear 
% Set directory where FASTER data is found
datadir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\Tmp\FASTER\';
savedir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\Tmp\FASTER\FASTER_Rejection.csv';
scriptdir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\ProcessingScripts\';

includedir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\CleanedData\';
excludedir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\ExcludedData\';

addpath(scriptdir)
cd(datadir)
subdirs = dir('*EogCleaned');

%% Create a csv file of the FASTER logs for each participant.

ss_num = size(subdirs, 1);
fprintf('You have %d FASTER log files to analyse\n', ss_num)

Subject =  cell(ss_num, 1);
Date =  cell(ss_num, 1);
Rejected =  cell(ss_num, 1);
Interpolated =  cell(ss_num, 1);
Output =  cell(ss_num, 4);

for i=1:ss_num
    logfile = dir(strcat(subdirs(i).name, '/*.log'));
    logfile_contents = fileread(strcat(logfile.folder, '/', logfile.name));
    expression = 'See ';
    [IdStartIndex, IdStopIndex] = regexp(logfile_contents,expression);
    Subject{i} = logfile_contents(IdStopIndex+1:IdStopIndex+3);
    Datecell =  regexp(logfile_contents,'[^\n\r]+2021 ','match');
    Date{i} = Datecell{end}(1:end-1);
    reject_log = regexp(logfile_contents,'Rejected \d+ [^\n\r]+','match');
    Rejected(i) = reject_log(end); % Take the most recent log entry
    if isempty(regexp(logfile_contents,'Interpolated channels \d+[^\n]+','match')) 
        Interpolated(i) = {'None'};
    else
    interpolation_log = regexp(logfile_contents,'Interpolated channels \d+[^\n]+','match');
    Interpolated(i) = interpolation_log(end);
    end
    Output(i,:) = [Subject(i), Date(i), Rejected(i), Interpolated(i)];
end
Output = cell2table(Output);
Output.Properties.VariableNames = {'Subject' 'Date' 'Rejected_Epochs' 'Interpolated_Channels'};
writetable(Output, savedir);

fprintf('Complete!\n')

%% Determine the percentage of epochs remaining per participant

folder_list = dir('*_EogCleaned');
folder_list = {folder_list.name};

fprintf('You have cleaned %d participants\n', numel(folder_list))

Conditions=[81;82;83;84;91;92;93;94];
EpochperCond = nan(numel(Conditions), numel(folder_list));
% Each of our 8 conditions has at least 30% of clean epochs per subject. 
rejection_threshold = 0.30; 

percentageEpoch = [];

for folder_index = 1 : numel(folder_list)
    file_info = dir(strcat(folder_list{folder_index}, '\*.set'));
    file_to_process = strcat(file_info.folder, '\', file_info.name);
    
    EEG = pop_loadset(file_to_process);
    EventTable = struct2table(EEG.event);
    AllConds = EventTable.type;
    EpochperCond(:,folder_index) = histc(AllConds, Conditions);
    
    participant_is_kept = true;
    for condition_index = 1 : numel(Conditions)
        percentageEpoch(condition_index, folder_index) = ...
            EpochperCond(condition_index,folder_index) / (320 / 8);
        if percentageEpoch(condition_index, folder_index) < rejection_threshold
            participant_is_kept = false;
        end
    end
       
    if participant_is_kept
        copyfile(file_to_process, strcat(includedir, file_info.name(1:3), '_Cleaned.set'));
    else
        copyfile(file_to_process, strcat(excludedir, file_info.name(1:3), '_Cleaned.set'));
    end
      
    variable_names{folder_index} = strcat('Participant_', file_info.name(1:3));
end

% Print a summary of the results
% percentageEpoch = array2table(percentageEpoch);
% percentageEpoch.Properties.VariableNames = variable_names