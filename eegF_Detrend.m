% EEG=eegF_Detrend(EEG,TimeRange)
%
% If a time-range is specified, only the data in that time-range is
% considered for the calculation of a linear regression subtracted from the
% entire epoch. TimeRange is a vector with two values: the start-time and
% the end-time of the to-be considered time-range. If TimeRange is omitted
% the entire epoch is used calculation of the linear regression.
%
% (c) 2005,2007,2008,2012 - S.K.Andersen
function EEG=eegF_Detrend(EEG,TimeRange)
if nargin<1 || nargin>2 || (nargin>1 && ~ismember(numel(TimeRange),[0 2]))
    help(mfilename)
    return
end
if nargin<2 || numel(TimeRange)==0, TimeRange=[EEG.xmin EEG.xmax]; end
disp('Detrending data...')
SPRange=eegF_Time2Sp(EEG,TimeRange(1)):eegF_Time2Sp(EEG,TimeRange(2));
for Channel=1:EEG.nbchan
    EEG.data(Channel,:,:)=ChannelDetrend(permute(EEG.data(Channel,:,:),[2 3 1]),SPRange);
end
% --- SUBFUNCTIONS ---
function EEGChannelData=ChannelDetrend(EEGChannelData,SPRange)
x=EEGChannelData(SPRange,:);
N=size(x,1);
M=size(EEGChannelData,1);
% Build regressor with linear piece + DC
a=[zeros(N,1,class(x)) ones(N,1,class(x))];
a(:,1)=(1:N)'/N;
b=[zeros(M,1,class(x)) ones(M,1,class(x))];
b(:,1)=((1:M)-SPRange(1)-1)'/N;
EEGChannelData = EEGChannelData-b*(a\x); % Remove best fit