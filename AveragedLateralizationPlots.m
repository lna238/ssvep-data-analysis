function AveragedLateralizationPlots(alina_participants, new_participants, Title)

    x = -1000 : 1000/256 : 7999;
    hEOGchannel = 66; %channel of interest
    Conditions=[81;82;83;84;91;92;93;94]; %triggers for conds
    ConditionNames = {'NegL2';'NegL25';'NegR2';'NegR25';'PosL2';'PosL25';'PosR2';'PosR25'};
    leftconds = [1,2,5,6];
    rightconds = [3,4,7,8];
    
    n_samples = 2304;
    
    total_number_of_epochs = 0;
    total_left_epochs = 0;
    total_right_epochs = 0;

    alina_y_left = zeros(n_samples,  numel(alina_participants));
    alina_y_right = zeros(n_samples,  numel(alina_participants));
    for i_ss = 1 : numel(alina_participants)

        EEG = pop_loadset('filename',alina_participants(i_ss).name,'filepath',alina_participants(i_ss).folder);
        total_number_of_epochs = total_number_of_epochs + EEG.trials;
        
        EEG2 = pop_selectevent(EEG,'type',Conditions(leftconds));
        total_left_epochs = total_left_epochs + EEG2.trials;
        alina_y_left(:, i_ss) = mean(squeeze(EEG2.data(hEOGchannel,:,:)),2);
        
        EEG2 = pop_selectevent(EEG,'type',Conditions(rightconds));
        total_right_epochs = total_right_epochs + EEG2.trials;
        alina_y_right(:, i_ss) = mean(squeeze(EEG2.data(hEOGchannel,:,:)),2);
    end
    
    new_y_left = zeros(n_samples,  numel(new_participants));
    new_y_right = zeros(n_samples,  numel(new_participants));
    for i_ss = 1 : numel(new_participants)

        EEG = pop_loadset('filename',new_participants(i_ss).name,'filepath',new_participants(i_ss).folder);
        EEG2 = pop_selectevent(EEG,'type',Conditions(leftconds));
        new_y_left(:, i_ss) = mean(squeeze(EEG2.data(hEOGchannel,:,:)),2);
        EEG2 = pop_selectevent(EEG,'type',Conditions(rightconds));
        new_y_right(:, i_ss) = mean(squeeze(EEG2.data(hEOGchannel,:,:)),2);
    end    
    
    figure('Name', strcat(Title,'hEOG plots'), 'Color', 'w', 'Units', 'Normalized');
    subplot(2,1,1);
    alina_left_averaged =  mean(alina_y_left,2);
    plot(x, alina_left_averaged)
    hold on
    alina_right_averaged =  mean(alina_y_right,2);
    plot(x, alina_right_averaged) 
    box off 
    set(gca, 'tickdir', 'out');
    xlim([-1000 8000])  
    ylim([-5 10])
    xlabel('Time points /ms') 
    ylabel('EOG Amplitude /mV') 
    legend('Left','Right')
    title("Alina Averaged hEOGs")    
    
%     max_of_grand_average_new_alina_left = max(abs(alina_left_averaged))
%     max_of_grand_average_new_alina_right = max(abs(alina_right_averaged))
%     epochs_per_subject = total_number_of_epochs /numel(alina_participants)
%     left_epochs_per_subject = total_right_epochs / numel(alina_participants)
%     right_epochs_per_subject = total_left_epochs / numel(alina_participants)

    subplot(2,1,2);
    new_left_averaged = mean(new_y_left,2);
    plot(x, new_left_averaged)
    hold on
    new_right_averaged = mean(new_y_right,2);
    plot(x, new_right_averaged) 
    box off 
    set(gca, 'tickdir', 'out');
    xlim([-1000 8000]) 
    ylim([-15 2])
    xlabel('Time points /ms') 
    ylabel('EOG Amplitude /mV') 
    legend('Left','Right')
    title("New Averaged hEOGs")    
end

