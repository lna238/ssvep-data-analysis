function [amplitude] = GetAmplitudeFromTable(table, participant_id, stimulus_frequency, stimulus_side, attention)
    
    % Get the amplitude when the stimulus is a positive emotion
    index = ismember(table.freqData, stimulus_frequency) & ... 
            ismember(table.sideData, stimulus_side) & ...
            ismember(table.subjData, participant_id) & ... 
            ismember(table.attData, attention) & ...
            ismember(table.emoData, 'Pos');
    positive_emotion_amplitude = table.Amplitude(index);
        
    % Get the amplitude when the stimulus is a negative emotion
    index = ismember(table.freqData, stimulus_frequency) & ... 
            ismember(table.sideData, stimulus_side) & ...
            ismember(table.subjData, participant_id) & ... 
            ismember(table.attData, attention) & ...
            ismember(table.emoData, 'Neg');
    negative_emotion_amplitude = table.Amplitude(index);

    amplitude = (positive_emotion_amplitude + negative_emotion_amplitude) / 2;
end

