%%%%%%%%%%%%%%%%%%%        SSVEP PROJECT 2020         %%%%%%%%%%%%%%%%%%%  
% Created by Lna.A - May 2021

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Set up environment and script global variables

clc 
clear 

datadir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\Tmp\EogCleaned';
scriptdir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\ProcessingScripts';
savedir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\Tmp\EogCleaned_Reepoched';
group_info = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\HEOG_plots_group_info.csv';
manual_cleaning_information = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\manual_cleaning_information.csv';
cd 'D:\EEGLAB\eeglab2019_1\'
eeglab;
addpath('D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\ProcessingScripts\Shared_Functions');


EMOTIONS =        {'HAS' 'SUS' 'ANS' 'DIS'};
EMOTION_CLASSES = {'POS' 'POS' 'NEG' 'NEG'};
TRIGGERS =        {'21'  '22'  '23'  '24'};
TRIGGERS_INT =    [ 21    22    23    24 ];

%% Re-epoch

cd(datadir)
file_list = dir('*.set');
file_list = {file_list.name};

for i = 1:length(file_list)
    EEG = pop_loadset('filename', file_list{i}, 'filepath', datadir);
        
    % STEP 5: Segment
    EEG = pop_epoch(EEG, TRIGGERS, [-0.200 1]);
    
    % STEP 6:  Baseline Correction 
    EEG = pop_rmbase(EEG, [-100 0]); 

    % save dataset for next step
    savename = [strrep(file_list{i},'.set','_'), 'Reepoched.set'];
    EEG = pop_saveset(EEG, 'filename', savename, 'filepath', savedir);

end

%% Averaging

datadir = 'D:\PROJECT_SSVEP\SSVEP2\SimplifiedPipeline\Tmp\EogCleaned_Reepoched';

cd(datadir)
file_list = dir('*.set');
file_list = {file_list.name};

for i = 1:length(file_list)
    EEG = pop_loadset(file_list{i});

    % Create a matrix to store each condition, electrodes, time, and subjects   
    for trigger_index = 1:length(TRIGGERS_INT)
        EEG_TMP = pop_selectevent(EEG, 'type', TRIGGERS_INT(trigger_index));
        
        % Create a matrix to store each condition, electrodes, time, and subjects
        averaged_data(trigger_index, :, :, i) = mean(EEG_TMP.data, 3);
    end
end

%% Count Epochs

fprintf('You have cleaned %d participants\n', numel(file_list))

EpochPerTrigger = nan(numel(TRIGGERS_INT), numel(file_list));
participant_id = cell(1, numel(file_list));

for file_index = 1 : numel(file_list)
    EEG = pop_loadset(file_list{file_index});
    participant_id{file_index} = strcat('participant_',file_list{file_index}(1:3));
    
    EventTable = struct2table(EEG.event);
    AllConds = EventTable.type;
    EpochPerTrigger(:,file_index) = histc(AllConds, TRIGGERS_INT);
end

% Print a summary of the results
array2table(EpochPerTrigger, ...
            'VariableNames', participant_id,...
            'RowNames', strcat('trigger_',TRIGGERS))