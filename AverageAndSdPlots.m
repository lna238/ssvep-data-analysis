function AverageAndSdPlots(files)

    x = -1000 : 1000/256 : 7999;
    hEOGchannel = 66; %channel of interest
    Conditions=[81;82;83;84;91;92;93;94]; %triggers for conds
    ConditionNames = {'NegL2';'NegL25';'NegR2';'NegR25';'PosL2';'PosL25';'PosR2';'PosR25'};
    leftconds = [1,2,5,6];
    rightconds = [3,4,7,8];

    n_samples = 2304;

    y_left = zeros(n_samples,  numel(files));
    y_right = zeros(n_samples,  numel(files));
    for i_ss = 1 : numel(files)
        EEG = pop_loadset('filename',files(i_ss).name,'filepath',files(i_ss).folder);
        EEG2 = pop_selectevent(EEG,'type',Conditions(leftconds));
        y_left(:, i_ss) = mean(squeeze(EEG2.data(hEOGchannel,:,:)),2);
        EEG2 = pop_selectevent(EEG,'type',Conditions(rightconds));
        y_right(:, i_ss) = mean(squeeze(EEG2.data(hEOGchannel,:,:)),2);
    end

    y_left_averaged =  mean(y_left,2);
    y_left_plus_std = y_left_averaged + std(y_left,0,2);
    y_left_minus_std = y_left_averaged - std(y_left,0,2);
    
    y_right_averaged =  mean(y_right,2);
    y_right_plus_std = y_right_averaged + std(y_right,0,2);
    y_right_minus_std = y_right_averaged - std(y_right,0,2);
    
    plot(x, y_left_averaged,'linewidth', 5, 'color', 'b');
    hold on
    plot(x, y_left_plus_std,'linewidth', 1,  'color', 'b');
    hold on
    plot(x, y_left_minus_std,'linewidth', 1, 'color', 'b');
    hold on
    
    plot(x, y_right_averaged,'linewidth', 5, 'color', 'r');
    hold on
    plot(x, y_right_plus_std,'linewidth', 1, 'color', 'r');
    hold on
    plot(x, y_right_minus_std,'linewidth', 1, 'color', 'r');
    
    box off 
    set(gca, 'tickdir', 'out');
    xlim([-1000 8000]) 
    ylim([-40,40])
    xlabel('Time points /ms') 
    ylabel('EOG Amplitude /mV') 
    legend('Attend Left','Attend Left +1SD', 'Attend Left -1SD', 'Attend Right', 'Attend Right +1SD', 'Attend Right -1SD')  
end