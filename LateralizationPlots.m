function LateralizationPlots(ParticipantsOfInterest, Title)
%LateralizationPlots function is used to easily plot all the participant hEOGs before and after drift
%cleaning instead of scripting them one by one. 

    x = -1000 : 1000/256 : 7999;
    hEOGchannel = 66; %channel of interest
    Conditions=[81;82;83;84;91;92;93;94]; %triggers for conds
    ConditionNames = {'NegL2';'NegL25';'NegR2';'NegR25';'PosL2';'PosL25';'PosR2';'PosR25'};
    leftconds = [1,2,5,6];
    rightconds = [3,4,7,8];


    figure('Name', strcat(Title,'hEOG plots'), 'Color', 'w', 'Units', 'Normalized');
    for i_ss = 1 : numel(ParticipantsOfInterest)

        subplot(6,6,i_ss); 
        EEG = pop_loadset('filename',ParticipantsOfInterest(i_ss).name,'filepath',ParticipantsOfInterest(i_ss).folder);
        EEG2 = pop_selectevent(EEG,'type',Conditions(leftconds));
        y = mean(squeeze(EEG2.data(hEOGchannel,:,:)),2);
        plot(x, y)
        hold on
        EEG2 = pop_selectevent(EEG,'type',Conditions(rightconds));
        y = mean(squeeze(EEG2.data(hEOGchannel,:,:)),2);
        plot(x, y)
        box off 
        set(gca, 'tickdir', 'out');
        xlim([-1000 8000])  
        xlabel('Time points /ms') 
        ylabel('EOG Amplitude /mV') 
        legend('Left)','Right')
        title(strcat("Participant ",ParticipantsOfInterest(i_ss).name(1:3))) 
    end 
end

