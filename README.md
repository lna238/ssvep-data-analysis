# SSVEP Data Analysis

This project contains all the MATLAB scripts written to analyze, using the EEGLAB toolbox, EEG signals from their raw data state to steady state visually evoked potentials. 

The steps involved include: 

* Downsamplimg the EEG signal 
* Detrending 
* Segmenting the signal into epochs
* Baseline correction
* Manual interpolation
* Fast-Fourier transformation
* Statistical and exploratory analyses


